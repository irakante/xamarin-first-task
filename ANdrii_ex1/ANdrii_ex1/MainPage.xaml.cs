﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ANdrii_ex1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            blueBox.Color = Color.Black;
        }
        private void Button2_Clicked(object sender, EventArgs e)
        {
            blueBox.Color = Color.Blue;
        }
    }
}
